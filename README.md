# System Monitor

Project consists of two subprojects. It provides service to collect data about system state and CLI client to query information.

## Language

C++ 17 mostly STL library with exception for accessing system information data and IPC.

## Build and Run

Clone repo

```
mkdir .build
cd .build
cmake ../
make -j8
system_monitor_client/system_monitor_client -h
system_monitor_service/system_monitor_service -h

```
## Description

Project has uses types of IPC

Description data constists of POSIX interprocess read/write lock, current index in circular samples data buffer, samples buffer size.
Sample element contains: CPU load, used memory, process count, thread count, timestamp.


1. Client and service use Linux shared memory as an IPC. Queries are run by clients.
Service creates shared memory in `/dev/shm/rimac_systemmonitor`, allocates space for description structure and samples data.
- What remains unknown is that I haven't found a solution for unlocking of locked pthread_rwlock if app crashes and doesn't unlock it.

2. Client and service use TCP/IP sockets.
Service binds 127.0.0.1:8888 and waits for connection. At the same time monitor loop is running and updates circular loop in parallel thread.
Client connects and sends request, waits for answer and disconnects.


**Data Sources**

- CPU load is read by parsing of the first line `/proc/stat` and comparing two subsequnt points. This doesn't provide high resolution results as Linux's jiffy scale is usually 10ms. It means that quering faster or near won't give any result. So limit for min value is introduced
- number of threads is read from sysinfo
- memory usage info is parsed and calculated from `/proc/meminfo`
- process number is parsed by listing of `/proc` subfolders.

## Sockets ##
**Service routines**

1. read sample data in a separate thread
2. lock rw lock
3. update sample and move index in circular buffer
4. unlock rw lock
5. sleep for sample timeout
6. on connection wait for request and start thread from a pool. lock read/write lock, run query unlock. If no threads available, wait.

**Client**

1. connect
2. send query
3. wait answer
4. prit result


## Shared memory ##
**Service loop**

1. read sample data
2. lock rw lock
3. update sample and move index in circular buffer
4. unlock rw lock
5. sleep for sample timeout

**Client routine**

1. lock rw lock for reading
2. iterate ower data
3. unlock rw lock

## Usage

**Service**
| flag | parameter  | description|
|------|------------|------------|
| -t | <period_ms>| Sets time period in ms for stored samples. Default: 1000 |
| -s | <sample_hz>|Sets sample frequency in Hz. Default: 24|
| -v |  | prints table of samples|
| --shmem |  | use shared memory|
| --socket |  | use shared memory|

**Client**

| flag | parameter  | description|
|------|------------|------------|
| -q | max-threads| prints max number of threads for the whole period|
| -q | min-threads| prints min number of threads for the whole period|
| -q | min-cpu [from to]| prints min CPU usage for the whole period. If from and to are defined, qeries over [from to] period|
| -q | max-cpu [from to]| prints max CPU usage for the whole period. If from and to are defined, qeries over [from to] period|
| -q | max-mem [from to]| prints min used memory for the whole period. If from and to are defined, qeries over [from to] period|
| -q | max-mem [from to]| prints max used memory for the whole period. If from and to are defined, qeries over [from to] period|
| -v |  | prints table of samples|
| --shmem |  | use shared memory|
| --socket |  | use shared memory|

```
./system_monitor_client -q min-cpu -v
[ CPU ][ MEM ][PROC ][  TH ][TIME ]
[  2.857][  4.816][    420][   2754][1653508904102][    17]
[  3.030][  4.816][    420][   2754][1653508904056][    16]
[ 18.919][  4.809][    420][   2755][1653508904012][    15]
[ 16.216][  4.788][    420][   2755][1653508903969][    14]
[  0.000][  4.788][    420][   2755][1653508903924][    13]
[  2.778][  4.788][    420][   2755][1653508903878][    12]
[  2.778][  4.788][    420][   2755][1653508903832][    11]
[  2.703][  4.788][    420][   2755][1653508903787][    10]
[  2.778][  4.788][    420][   2755][1653508903741][     9]
[  0.000][  4.788][    420][   2755][1653508903695][     8]
[ 14.286][  4.788][    420][   2755][1653508903649][     7]
[  8.333][  4.786][    420][   2754][1653508903605][     6]
[  2.778][  4.786][    420][   2752][1653508903558][     5]
[  0.000][  4.786][    420][   2752][1653508903512][     4]
[  2.778][  4.786][    420][   2752][1653508903466][     3]
[  2.703][  4.786][    420][   2752][1653508903420][     2]
[  2.778][  4.813][    420][   2752][1653508903376][     1]
[  7.895][  4.813][    420][   2752][1653508903330][     0]
[  2.778][  4.813][    420][   2752][1653508903284][    23]
[  0.000][  4.813][    420][   2752][1653508903238][    22]
[  2.703][  4.813][    420][   2752][1653508903193][    21]
[  2.778][  4.813][    420][   2752][1653508903147][    20]
[  8.108][  4.813][    420][   2752][1653508903101][    19]
[  8.571][  4.813][    420][   2752][1653508903056][    18]
min cpu total:   0.00% at: 25-05-2022 20:01:43:.512 1653508903512

```

## Further improvements

- more precise service loop: calculate read time and sleep for (Tsample - Tread)
- code cleanup
- find and move common code to the library
- add tests
- improve error handling. Lots of methods may throw exception
- test for high number of clients
- profile memory and CPU
- check sockets code
