#include "systemmonitorclient.h"
#include "shareddata.h"
#include "systemmonitorreader.h"

#include <charconv>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <optional>
#include <utility>

#include <fcntl.h>
#include <unistd.h>

#include <pthread.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include<sys/socket.h>
#include<netinet/in.h>
#include <arpa/inet.h>

bool SystemMonitorClientShmem::setup() {
    // O_RDWR as we need to write to rw lock
    m_shmFixed = shm_open(SHARED_MEMORY_OBJECT_NAME, O_RDWR, 0777);
    if (m_shmFixed == -1) {
        std::cout << "shm_open error" << std::endl;
        return false;
    }

    // get size of description struct + samples array
    struct stat memStat;
    int rc = fstat(m_shmFixed, &memStat);
    if (rc == -1) {
        std::cout << "fstat error" << std::endl;
        return false;
    }

    m_totalByteSize = memStat.st_size;
    // PROT_WRITE as we need to write to rw lock
    void* p = mmap(0, memStat.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, m_shmFixed, 0);
    if (p == (void*)-1) {
        std::cout << "mmap error" << std::endl;
        return false;
    }

    m_sharedMem = static_cast<SharedData*>(p);
    if (((memStat.st_size - sizeof(SharedData)) / sizeof(ProcessSample) >= 1)
            && ((memStat.st_size - sizeof(SharedData)) % sizeof(ProcessSample) == 0)) {
        m_sharedSamples = reinterpret_cast<ProcessSample*>(&m_sharedMem[1]);
    } else {
        std::cout << "wrong memory size" << std::endl;
        return false;
    }

    return true;
}

void SystemMonitorClientShmem::closeClient() {
    munmap(m_sharedMem, m_totalByteSize);
    close(m_shmFixed);
}

void SystemMonitorClientBase::dumpProcessSamples(const ProcessSample *samplesData, unsigned int size, unsigned int index,
                                             std::optional<std::pair<unsigned long, unsigned long>> fromTo) {
    std::cout << "[" << std::setw(6) << "CPU ]"
              << "[" << std::setw(6) << "MEM ]"
              << "[" << std::setw(6) << "PROC ]"
              << "[" << std::setw(6) << "TH ]"
              << "[" << std::setw(6) << "TIME ]" << std::endl;

    std::function<void(unsigned int i)> print = [samplesData, fromTo](unsigned int i) {
        std::cout << "[" << std::setw(7) << std::fixed << std::setprecision(3) << samplesData[i].lastCPULoad << "]"
                  << "[" << std::setw(7) << samplesData[i].lastMemoryUsed << "]"
                  << "[" << std::setw(7) << samplesData[i].lastProcessCount << "]"
                  << "[" << std::setw(7) << samplesData[i].lastThreadCount << "]"
                  << "[" << samplesData[i].timeStamp << "]"
                  << "[" << std::setw(6) << i << "]";
        if (fromTo.has_value()) {
            if ((samplesData[i].timeStamp <= fromTo->first) && ((samplesData[i].timeStamp >= fromTo->second))) {
                std::cout << "[*]";
            } else {
                std::cout << "[-]";
            }
        }
        std::cout << std::endl;
    };

    for (int i = index; i >= 0; --i) {
        print(i);
    }
    for (unsigned int i = (size - 1); i > index; --i) {
        print(i);
    }
}

void SystemMonitorClientBase::setDump(bool newDump)
{
    m_dump = newDump;
}

void SystemMonitorClientShmem::runQuery(QueryType query, unsigned long from, unsigned long to) const {
    SystemMonitorReader reader(m_sharedSamples, m_sharedMem, m_sharedMem->samplesCount);

    int rc = pthread_rwlock_rdlock(&m_sharedMem->samplesDataRwLock);
    if (rc == 0) {
        bool rangeCheckPasses{true};
        std::optional<std::pair<unsigned long, unsigned long>> fromTo;
        switch (query) {
        case QueryType::MAX_CPU_RANGE:  // fallthrough
        case QueryType::MIN_CPU_RANGE:  // fallthrough
        case QueryType::MAX_MEM_RANGE:  // fallthrough
        case QueryType::MIN_MEM_RANGE: {
            fromTo = {from, to};
            //if ((samplesData[i].timeStamp <= fromTo->first) && ((samplesData[i].timeStamp >= fromTo->second))) {
            unsigned long last = m_sharedSamples[m_sharedMem->index].timeStamp;
            unsigned long first;
            if (m_sharedMem->index == (m_sharedMem->samplesCount - 1)) {
                first = m_sharedSamples[0].timeStamp;
            } else {
                first = m_sharedSamples[m_sharedMem->index + 1].timeStamp;
            }

            if (last < from && last < to) {
                rangeCheckPasses = false;
            }

            if (first > from && first > to) {
                rangeCheckPasses = false;
            }

            if (from < to) {
                rangeCheckPasses = false;
            }

            if (!rangeCheckPasses) {
                std::cout << "out of range: " << last << "|" << first << std::endl;
            }

            break;
        }
        default: {
            // no need to check
            break;
        }
        }

        if (m_dump) {
            dumpProcessSamples(m_sharedSamples, m_sharedMem->samplesCount, m_sharedMem->index, fromTo);
        }

        if (rangeCheckPasses) {
            unsigned int index{0};
            switch (query) {
            case QueryType::NO_QUERY: {
                std::cout << "no query" << std::endl;
                break;
            }
            case QueryType::MAX_THREADS: {
                index = reader.getMaxThreadCountIndex();
                std::cout << "max thread count: " << m_sharedSamples[index].lastThreadCount;
                break;
            }
            case QueryType::MIN_THREADS: {
                index = reader.getMinThreadCountIndex();
                std::cout << "min thread count: " << m_sharedSamples[index].lastThreadCount;
                break;
            }
            case QueryType::MAX_CPU_RANGE: {
                index = reader.getMaxCPULoadIndex(from, to);
                std::cout << "max cpu: " << std::fixed << std::setw(6) << std::setprecision(2)
                          << m_sharedSamples[index].lastCPULoad << "%";
                break;
            }
            case QueryType::MIN_CPU_RANGE: {
                index = reader.getMinCPULoadIndex(from, to);
                std::cout << "min cpu: " << std::fixed << std::setw(6) << std::setprecision(2)
                          << m_sharedSamples[index].lastCPULoad << "%";
                break;
            }
            case QueryType::MAX_CPU: {
                index = reader.getMaxCPULoadIndex();
                std::cout << "max cpu total: " << std::fixed << std::setw(6) << std::setprecision(2)
                          << m_sharedSamples[index].lastCPULoad << "%";
                break;
            }
            case QueryType::MIN_CPU: {
                index = reader.getMinCPULoadIndex();
                std::cout << "min cpu total: " << std::fixed << std::setw(6) << std::setprecision(2)
                          << m_sharedSamples[index].lastCPULoad << "%";
                if (m_sharedSamples[index].lastCPULoad == 0.0f) {
                    dumpProcessSamples(m_sharedSamples, m_sharedMem->samplesCount, m_sharedMem->index, fromTo);
                    std::cout << "zero:" << index;
                    sleep(10);
                }
                break;
            }
            case QueryType::MAX_MEM_RANGE: {
                index = reader.getMaxMemoryUsedIndex(from, to);
                std::cout << "max mem: " << std::fixed << std::setw(7) << std::setprecision(3)
                          << m_sharedSamples[index].lastMemoryUsed << " GiB" << std::endl;
                break;
            }
            case QueryType::MIN_MEM_RANGE: {
                index = reader.getMinMemoryUsedIndex(from, to);
                std::cout << "min mem: " << std::fixed << std::setw(7) << std::setprecision(3)
                          << m_sharedSamples[index].lastMemoryUsed << " GiB";
                break;
            }
            case QueryType::MAX_MEM: {
                index = reader.getMaxMemoryUsedIndex();
                std::cout << "max mem total: " << std::fixed << std::setw(7) << std::setprecision(3)
                          << m_sharedSamples[index].lastMemoryUsed << " GiB";
                break;
            }
            case QueryType::MIN_MEM: {
                index = reader.getMinMemoryUsedIndex();
                std::cout << "min mem total: " << std::fixed << std::setw(7) << std::setprecision(3)
                          << m_sharedSamples[index].lastMemoryUsed << " GiB";
                break;
            }
            }

            std::cout << " at: " << timeToString(std::chrono::time_point<std::chrono::system_clock>(std::chrono::milliseconds(
                                                                                                        m_sharedSamples[index].timeStamp)))
                      << " " << m_sharedSamples[index].timeStamp << std::endl;
        }

        pthread_rwlock_unlock(&m_sharedMem->samplesDataRwLock);
    } // read lock ok
}


std::string SystemMonitorClientBase::timeToString(const std::chrono::system_clock::time_point &time_point) {
    std::time_t tt = std::chrono::system_clock::to_time_t(time_point);
    std::tm tm = *std::gmtime(&tt); //GMT (UTC)
    std::stringstream ss;
    ss << std::put_time( &tm, "%d-%m-%Y %H:%M:%S:");
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_point.time_since_epoch()) % 1000;
    ss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    return ss.str();
}

SystemMonitorClientBase* SystemMonitorClientBase::parse(const std::vector<std::string_view> &args,
                                                       QueryType &query, unsigned long &from, unsigned long &to) {
    SystemMonitorClientBase* result{nullptr};
    SystemMonitorClientBase::MonitorType monitorType{SystemMonitorClientBase::MonitorType::Socket};
    query = QueryType::NO_QUERY;
    bool dump{false};

    if (args.size() == 9) {
        printUsage();
        return nullptr;
    }

    for (unsigned int i = 0; i < args.size(); ++i) {
        if ((args[i] == ARG_QUERY) && (i + 1) < args.size()) {
            ++i;
            if (args[i] == ARG_MAX_THREADS) {
                query = QueryType::MAX_THREADS;
            } else if (args[i] == ARG_MIN_THREADS) {
                query = QueryType::MIN_THREADS;
            } else if (args[i] == ARG_MAX_CPU) {
                if ((i + 2) < args.size()) {
                    query = QueryType::MAX_CPU_RANGE;
                    std::from_chars(args[i+1].data(), args[i+1].data() + args[i+1].size(), from);
                    std::from_chars(args[i+2].data(), args[i+2].data() + args[i+2].size(), to);
                    i += 2;
                } else {
                    query = QueryType::MAX_CPU;
                }
            } else if (args[i] == ARG_MIN_CPU) {
                if ((i + 2) < args.size()) {
                    query = QueryType::MIN_CPU_RANGE;
                    std::from_chars(args[i+1].data(), args[i+1].data() + args[i+1].size(), from);
                    std::from_chars(args[i+2].data(), args[i+2].data() + args[i+2].size(), to);
                    i += 2;
                } else {
                    query = QueryType::MIN_CPU;
                }
            } else if (args[i] == ARG_MIN_MEM) {
                if ((i + 2) < args.size()) {
                    query = QueryType::MIN_MEM_RANGE;
                    std::from_chars(args[i+1].data(), args[i+1].data() + args[i+1].size(), from);
                    std::from_chars(args[i+2].data(), args[i+2].data() + args[i+2].size(), to);
                    i += 2;
                } else {
                    query = QueryType::MIN_MEM;
                }
            } else if (args[i] == ARG_MAX_MEM) {
                if ((i + 2) < args.size()) {
                    query = QueryType::MAX_MEM_RANGE;
                    std::from_chars(args[i+1].data(), args[i+1].data() + args[i+1].size(), from);
                    std::from_chars(args[i+2].data(), args[i+2].data() + args[i+2].size(), to);
                    i += 2;
                } else {
                    query = QueryType::MAX_MEM;
                }
            } else {
                // nothing to do
            }
        } else if (args[i] == ARG_HELP) {
            printUsage();
        } else if (args[i] == ARG_VERBOSE) {
            dump = true;
        } else if ((args[i] == ARG_SOCKET)) {
            monitorType = SystemMonitorClientBase::MonitorType::Socket;
        } else if ((args[i] == ARG_SHMEM)) {
            monitorType = SystemMonitorClientBase::MonitorType::SharedMemory;
        } else {
            // nothing to do
        }
    }

    if (monitorType == SystemMonitorClientBase::MonitorType::Socket) {
        result = new SystemMonitorClientSocket();
    } else if (monitorType == SystemMonitorClientBase::MonitorType::SharedMemory) {
        result = new SystemMonitorClientShmem();
    } else {

    }

    if (result != nullptr) {
        result->setDump(dump);
    }

    return result;
}

void SystemMonitorClientBase::printUsage() {
    std::cout << "usage: system_monitor_client [option]" << std::endl
              << "  options: " << std::endl
              << "    -q max-threads, max number of threads" << std::endl
              << "       min-threads, min number of threads" << std::endl
              << "       min-cpu [from to], min CPU usage, % [for period]" << std::endl
              << "       max-cpu [from to], max CPU usage, % [for period]" << std::endl
              << "       min-mem [from to], min used memory, GiB [for period]" << std::endl
              << "       max-mem [from to], max used memory, GiB [for period]" << std::endl
              << "    -v,  dumps samples" << std::endl
              << "    -h,  print this message" << std::endl;
}

bool SystemMonitorClientSocket::setup() {
    m_socket = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = inet_addr(SOCKET_ADDR);
    serverAddr.sin_port = htons(SOCKET_PORT);


    if (connect(m_socket, reinterpret_cast<sockaddr*>(&serverAddr), sizeof(serverAddr)) == -1) {
        std::cout << "error: socket connect" << std::endl;
        return false;
    }

    return true;
}


void SystemMonitorClientSocket::runQuery(QueryType queryType, unsigned long from, unsigned long to) const {
    Query query;
    query.queryType = queryType;
    query.from = from;
    query.to = to;

    if(send(m_socket, reinterpret_cast<void*>(&query), sizeof(Query), 0) == -1) {
        std::cout << "error: send socket" << std::endl;
        return;
    }

    Answer ans;
    if(recv(m_socket, reinterpret_cast<void*>(&ans), sizeof(Answer), 0) == -1) {
        std::cout << "error: send socket" << std::endl;
        return;
    }

    switch (queryType) {
    case QueryType::NO_QUERY: {
        std::cout << "no query" << std::endl;
        break;
    }
    case QueryType::MAX_THREADS: {
        std::cout << "max thread count: " << ans.sample.lastThreadCount;
        break;
    }
    case QueryType::MIN_THREADS: {
        std::cout << "min thread count: " << ans.sample.lastThreadCount;
        break;
    }
    case QueryType::MAX_CPU_RANGE: {
        std::cout << "max cpu: " << std::fixed << std::setw(6) << std::setprecision(2)
                  << ans.sample.lastCPULoad << "%";
        break;
    }
    case QueryType::MIN_CPU_RANGE: {
        std::cout << "min cpu: " << std::fixed << std::setw(6) << std::setprecision(2)
                  << ans.sample.lastCPULoad << "%";
        break;
    }
    case QueryType::MAX_CPU: {
        std::cout << "max cpu total: " << std::fixed << std::setw(6) << std::setprecision(2)
                  << ans.sample.lastCPULoad << "%";
        break;
    }
    case QueryType::MIN_CPU: {
        std::cout << "min cpu total: " << std::fixed << std::setw(6) << std::setprecision(2)
                  << ans.sample.lastCPULoad << "%";
        break;
    }
    case QueryType::MAX_MEM_RANGE: {
        std::cout << "max mem: " << std::fixed << std::setw(7) << std::setprecision(3)
                  << ans.sample.lastMemoryUsed << " GiB" << std::endl;
        break;
    }
    case QueryType::MIN_MEM_RANGE: {
        std::cout << "min mem: " << std::fixed << std::setw(7) << std::setprecision(3)
                  << ans.sample.lastMemoryUsed << " GiB";
        break;
    }
    case QueryType::MAX_MEM: {
        std::cout << "max mem total: " << std::fixed << std::setw(7) << std::setprecision(3)
                  << ans.sample.lastMemoryUsed << " GiB";
        break;
    }
    case QueryType::MIN_MEM: {
        std::cout << "min mem total: " << std::fixed << std::setw(7) << std::setprecision(3)
                  << ans.sample.lastMemoryUsed << " GiB";
        break;
    }
    }

    std::cout << " at: " << timeToString(std::chrono::time_point<std::chrono::system_clock>(std::chrono::milliseconds(
                ans.sample.timeStamp))) << " " << ans.sample.timeStamp << std::endl;

}
