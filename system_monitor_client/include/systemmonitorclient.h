#ifndef SYSTEMMONITORCLIENT_H
#define SYSTEMMONITORCLIENT_H

#include "shareddata.h"
#include <chrono>
#include <string_view>
#include <vector>
#include <optional>

class SystemMonitorClientBase {
public:
    enum class MonitorType {
        SharedMemory,
        Socket
    };

    static SystemMonitorClientBase* parse(const std::vector<std::string_view> &args,
                                          QueryType &query, unsigned long &from, unsigned long &to);
    static void printUsage();

    static std::string timeToString(const std::chrono::system_clock::time_point &time_point);
    static void dumpProcessSamples(const ProcessSample *samplesData,
                                   unsigned int size, unsigned int index,
                                   std::optional<std::pair<unsigned long, unsigned long>> fromTo);

    virtual ~SystemMonitorClientBase() = default;

    virtual bool setup() = 0;
    virtual void runQuery(QueryType query, unsigned long from, unsigned long to) const = 0;
    virtual void closeClient() = 0;

    static constexpr std::string_view ARG_SOCKET{"--socket"};
    static constexpr std::string_view ARG_SHMEM{"--shmem"};
    static constexpr std::string_view ARG_QUERY{"-q"};
    static constexpr std::string_view ARG_HELP{"-h"};
    static constexpr std::string_view ARG_VERBOSE{"-v"};
    static constexpr std::string_view ARG_MAX_THREADS{"max-threads"};
    static constexpr std::string_view ARG_MIN_THREADS{"min-threads"};
    static constexpr std::string_view ARG_MIN_CPU{"min-cpu"};
    static constexpr std::string_view ARG_MAX_CPU{"max-cpu"};
    static constexpr std::string_view ARG_MIN_MEM{"min-mem"};
    static constexpr std::string_view ARG_MAX_MEM{"max-mem"};

    static constexpr unsigned int SHMEM_RETRY_COUNT{10};

public:
    bool m_dump{false};
    void setDump(bool newDump);
};

class SystemMonitorClientSocket final : public SystemMonitorClientBase {
public:
    SystemMonitorClientSocket() = default;

    void runQuery(QueryType queryType, unsigned long from, unsigned long to) const override;
    // socket
    bool setup() override;
    void closeClient() override {}

private:
    int m_socket{-1};

private:
    SystemMonitorClientSocket(const SystemMonitorClientSocket &) = delete;
    SystemMonitorClientSocket(const SystemMonitorClientSocket &&) = delete;
    SystemMonitorClientSocket& operator = (const SystemMonitorClientSocket &) = delete;
    SystemMonitorClientSocket& operator = (const SystemMonitorClientSocket &&) = delete;
};

class SystemMonitorClientShmem final : public SystemMonitorClientBase {
public:
    SystemMonitorClientShmem() = default;

    // create and setup shared memory
    bool setup() override;
    // close shared memory
    void closeClient() override;
    void runQuery(QueryType query, unsigned long from, unsigned long to) const override;

private:
    SharedData* m_sharedMem{nullptr};
    ProcessSample *m_sharedSamples{nullptr};
    int m_shmFixed{0};
    unsigned int m_totalByteSize{0};

private:
    SystemMonitorClientShmem(const SystemMonitorClientShmem &) = delete;
    SystemMonitorClientShmem(const SystemMonitorClientShmem &&) = delete;
    SystemMonitorClientShmem& operator = (const SystemMonitorClientShmem &) = delete;
    SystemMonitorClientShmem& operator = (const SystemMonitorClientShmem &&) = delete;
};

#endif // SYSTEMMONITORCLIENT_H
