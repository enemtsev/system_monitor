#include <iostream>


#include <cstring>
#include <vector>
#include <string>
#include <string_view>

#include <sstream>

#include "systemmonitorclient.h"

int main(int argc, char **argv)
{
    SystemMonitorClientBase *client{nullptr};

    QueryType query;
    unsigned long from{0};
    unsigned long to{0};
    std::vector<std::string_view> args(argv + 1, argv + argc);

    client = SystemMonitorClientBase::parse(args, query, from, to);

    if (query == QueryType::NO_QUERY) {
        if (client != nullptr) {
            delete client;
        }
        SystemMonitorClientBase::printUsage();
        return EXIT_FAILURE;
    }

    if (!client->setup()) {
        return EXIT_FAILURE;
    }

    client->runQuery(query, from, to);

    client->closeClient();

    return EXIT_SUCCESS;
}
