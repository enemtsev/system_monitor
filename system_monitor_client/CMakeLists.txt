cmake_minimum_required(VERSION 3.5)

project(system_monitor_client LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(system_monitor_client
    ../common/src/systemmonitorreader.cpp
    src/systemmonitorclient.cpp
    main.cpp)

target_include_directories(system_monitor_client PRIVATE
    include
    ../common/include)
