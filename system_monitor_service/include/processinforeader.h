#ifndef PROCESSINFOREADER_H
#define PROCESSINFOREADER_H

#include <chrono>
#include <unordered_set>
#include <limits>
#include <vector>
#include <string>
#include "shareddata.h"
#include <atomic>
#include <thread>

#include <unistd.h>

class ProcessInfoReader {
public:
    ProcessInfoReader() = default;
    ~ProcessInfoReader() = default;

    float getLastCPULoad() const;
    float getLastMemoryUsed() const;
    unsigned int getLastProcessCount() const;
    unsigned int getLastThreadCount() const;
    const ProcessSample &getLastProcessSample() const;

    unsigned long getTimestamp() const;

    void update();

private:
    float readCpuLoad();
    float readUsedMemory() const;
    void readProcessInfo(unsigned int &processes, unsigned int &threads) const;
    std::vector<unsigned int> readPids() const;
    unsigned int readPidsCount() const;

    ProcessSample m_sample;

    unsigned long long m_lastCpuTotal{0};
    unsigned long long m_lastCpuBusy{0};

    std::chrono::time_point<std::chrono::system_clock> m_timestamp;

private:
    static constexpr char PROC_PATH[] = "/proc/";
    static constexpr char PROC_PID_STATUS_FILENAME[] = "/status";
    static constexpr char PROC_STATS_FILENAME[] = "/proc/stat";
    static constexpr char PROC_MEMINFO_FILENAME[] = "/proc/meminfo";

    static constexpr char MEMINFO_MEMTOTAL_ID[] = "MemTotal:";
    static constexpr char MEMINFO_MEMFREE_ID[] = "MemFree:";
    static constexpr char MEMINFO_BUFFERS_ID[] = "Buffers:";
    static constexpr char MEMINFO_CACHED_ID[] = "Cached:";
    static constexpr char MEMINFO_SRECLAIMABLE_ID[] = "SReclaimable:";
    static constexpr char MEMINFO_SHMEM_ID[] = "Shmem:";

    static constexpr char PROC_PID_STATUS_THREADS_ID[] = "Threads:";

    const std::unordered_set<std::string> m_meminfoRequiredFields {
        {MEMINFO_MEMTOTAL_ID},
        {MEMINFO_MEMFREE_ID},
        {MEMINFO_BUFFERS_ID},
        {MEMINFO_CACHED_ID},
        {MEMINFO_SRECLAIMABLE_ID},
        {MEMINFO_SHMEM_ID}
    };

private:
    ProcessInfoReader(const ProcessInfoReader &) = delete;
    ProcessInfoReader(const ProcessInfoReader &&) = delete;
    ProcessInfoReader& operator = (const ProcessInfoReader &) = delete;
    ProcessInfoReader& operator = (const ProcessInfoReader &&) = delete;
};

#endif // PROCESSREADER_H
