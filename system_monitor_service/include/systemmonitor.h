#ifndef SYSTEMMONITOR_H
#define SYSTEMMONITOR_H

#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "processinforeader.h"
#include "shareddata.h"

class SystemMonitorBase {
public:
    enum class MonitorType {
        SharedMemory,
        Socket
    };

    virtual ~SystemMonitorBase() = default;

    static SystemMonitorBase* parse(const std::vector<std::string_view> &args);
    static void printUsage();

    virtual bool setup() = 0;
    virtual void closeMonitor() = 0;
    virtual void exec() = 0;
    virtual void stop() = 0;


    void setSampleInterval_ms(unsigned int newSampleInterval_ms);

    void setTimePeriod_ms(unsigned int newTimePeriod_ms);

    void setSize(unsigned int newSize);

public:
    unsigned int m_sampleInterval_ms{DEFAULT_SAMPLE_INTERVAL_MS};
    unsigned int m_timePeriod_ms{DEFAULT_TIME_PERIOD_MS};

    ProcessInfoReader m_reader;

    bool m_run{false};
    bool m_dump{false};

    unsigned int m_index{0};
    unsigned int m_size{0};

    ProcessSample *m_sampleData{nullptr};
    SharedData *m_sharedData{nullptr};

    static constexpr std::string_view ARG_SOCKET{"--socket"};
    static constexpr std::string_view ARG_SHMEM{"--shmem"};
    static constexpr std::string_view ARG_PERIOD{"-t"};
    static constexpr std::string_view ARG_SAMPLE_HZ{"-s"};
    static constexpr std::string_view ARG_VERBOSE{"-v"};
    static constexpr std::string_view ARG_HELP{"-h"};

    static constexpr unsigned int DEFAULT_SAMPLE_INTERVAL_MS{42};
    static constexpr unsigned int DEFAULT_TIME_PERIOD_MS{1000};

    static constexpr unsigned int MIN_SAMPLE_INTERVAL_MS{30};

public:
    void dumpProcessSamples() const;
    void sampleLoop();

public:
    std::mutex m_sampleLoopWaitMutex;
    void setDump(bool newDump);
};

class SystemMonitorSockets final : public SystemMonitorBase {
public:
    SystemMonitorSockets() = default;
    bool setup() override;
    void closeMonitor() override;
    void exec() override;
    void stop() override;

private:
    void setupSocketMem();
    void clearSocketMem();
    bool setupSocket();
    void closeSocket();
    void execSocketServer();
    void startSamplerThread();

private:
    int m_serverSocket{-1};
    static constexpr int MAX_SOCKET_CONNECTIONS{40};
    std::thread m_socketConnectionThreads[MAX_SOCKET_CONNECTIONS];
    bool m_runSocketServer{true};
    std::thread m_samplerThread;

    std::mutex m_queryWiatMutex;
    std::condition_variable m_queryWaitCV;
    std::atomic<unsigned int> m_queryThreadCount{0};

    void queryThread(int socket);

private:
    SystemMonitorSockets(const SystemMonitorSockets &) = delete;
    SystemMonitorSockets(const SystemMonitorSockets &&) = delete;
    SystemMonitorSockets& operator = (const SystemMonitorSockets &) = delete;
    SystemMonitorSockets& operator = (const SystemMonitorSockets &&) = delete;
};

class SystemMonitorSharedMem final : public SystemMonitorBase {
public:
    SystemMonitorSharedMem() = default;

    bool setup() override;
    void exec() override;
    void stop() override;
    void closeMonitor() override;

private:
    static constexpr int m_fixedMemSize{sizeof(SharedData)};
    int m_shmFixed{0};

private:
    SystemMonitorSharedMem(const SystemMonitorSharedMem &) = delete;
    SystemMonitorSharedMem(const SystemMonitorSharedMem &&) = delete;
    SystemMonitorSharedMem& operator = (const SystemMonitorSharedMem &) = delete;
    SystemMonitorSharedMem& operator = (const SystemMonitorSharedMem &&) = delete;
};

#endif // SYSTEMMONITOR_H
