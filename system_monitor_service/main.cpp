#include <iostream>
#include <thread>
#include <cstring>
#include <string_view>

#include <signal.h>

#include "processinforeader.h"
#include "systemmonitor.h"
#include "shareddata.h"

SystemMonitorBase *systemMonitor{nullptr};

void signal_handler(int s) {
    std::cout << "SIGINT handler" << std::endl;
    if (systemMonitor != nullptr) {
        systemMonitor->stop();
    }
}

int main(int argc, char **argv) {
    struct sigaction sact;

    sigemptyset(&sact.sa_mask);
    sact.sa_flags = 0;
    sact.sa_handler = signal_handler;
    sigaction(SIGINT, &sact, NULL);

    std::vector<std::string_view> args(argv + 1, argv + argc);

    systemMonitor = SystemMonitorBase::parse(args);

    if (systemMonitor == nullptr) {
        return EXIT_FAILURE;
    }

    if (systemMonitor->setup()) {
        systemMonitor->exec();
        systemMonitor->closeMonitor();
    }

    delete systemMonitor;
    systemMonitor = nullptr;

    return 0;
}
