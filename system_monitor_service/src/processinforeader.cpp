#include "processinforeader.h"

#include <sys/sysinfo.h>

#include <algorithm>
#include <dirent.h>
#include <iostream>
#include <vector>
#include <filesystem>
#include <sstream>
#include <thread>
#include <fstream>
#include <optional>

float ProcessInfoReader::readUsedMemory() const {
    std::ifstream stream(PROC_MEMINFO_FILENAME);

    std::string id;
    std::string temp;
    std::string line;

    std::optional<unsigned long long> memTotal, memFree, buffers, cached, sReclaimable, shMem;
    bool ok{false};

    if (stream.is_open()) {
        while(!stream.eof()) {
            std::getline(stream, line);
            std::istringstream linestream(line);
            linestream >> id >> temp;

            if (!memTotal.has_value() && (id == MEMINFO_MEMTOTAL_ID)) {
                memTotal = std::stoi(temp);
            } else if (!memFree.has_value() && (id == MEMINFO_MEMFREE_ID)) {
                memFree = std::stoi(temp);
            } else if (!buffers.has_value() && (id == MEMINFO_BUFFERS_ID)) {
                buffers = std::stoi(temp);
            } else if (!cached.has_value() && (id == MEMINFO_CACHED_ID)) {
                cached = std::stoi(temp);
            } else if (!sReclaimable.has_value() && (id == MEMINFO_SRECLAIMABLE_ID)) {
                sReclaimable = std::stoi(temp);
            } else if (!shMem.has_value() && (id == MEMINFO_SHMEM_ID)) {
                shMem = std::stoi(temp);
            } else {
                // nothing to do
            }

            if (memTotal.has_value() && memFree.has_value() && cached.has_value() && sReclaimable.has_value()
                    && shMem.has_value()) {
                ok = true;
                break;
            }
        }
    }

    if (!ok) {
        std::cout << "warning: read used memory";
        return 0.0f;
    }

    return (*memTotal - *memFree - *buffers - *cached - *sReclaimable - *shMem) / 1048576.0f; // / 1024 / 1024
}

void ProcessInfoReader::readProcessInfo(unsigned int &processes, unsigned int &threads) const {
    processes = readPidsCount();

    struct sysinfo info;
    sysinfo(&info);
    threads = info.procs;

//    std::vector<unsigned int> pids = readPids();
//    processes = pids.size();
// takes 30 ms
//    threads = 0;
//    for (auto pid : pids) {
//        std::ifstream stream(PROC_PATH + std::to_string(pid) + PROC_PID_STATUS_FILENAME);
//        std::string line;
//        std::string id;
//        std::string temp;

//        if (stream.is_open()) {
//            while(!stream.eof()) {
//                std::getline(stream, line);
//                std::istringstream linestream(line);
//                linestream >> id >> temp;
//                if (id == PROC_PID_STATUS_THREADS_ID) {
//                    threads += std::stoi(temp);
//                    break;
//                }
//            }
//        }
//    }
}

std::vector<unsigned int> ProcessInfoReader::readPids() const {
    std::vector<unsigned int> pids;
    pids.reserve(m_sample.lastProcessCount);

    for (const auto &entry : std::filesystem::directory_iterator(PROC_PATH)) {
        if (entry.is_directory()) {
            std::string s = entry.path().stem();
            if (std::all_of(s.begin(), s.end(), ::isdigit)) {
                pids.push_back(std::stoi(entry.path().stem()));
            }
        }
    }

    return pids;
}

unsigned int ProcessInfoReader::readPidsCount() const {
    unsigned int count{0};

    try {
        for (const auto &entry : std::filesystem::directory_iterator(PROC_PATH)) {
            if (entry.is_directory()) {
                std::string s = entry.path().stem();
                if (std::all_of(s.begin(), s.end(), ::isdigit)) {
                    ++count;
                }
            }
        }
    } catch (std::filesystem::filesystem_error &ex) {
        std::cout << "erro: filesystem" << std::endl;
    }



    return count;
}

float ProcessInfoReader::readCpuLoad() {
    std::ifstream stream(PROC_STATS_FILENAME);
    float load{0.0f};
    std::string line;

    if (stream.is_open()) {
        std::getline(stream, line);
        if ((line.size() > 0) && (line.data()[0] == 'c')) {
            static constexpr char fmt[] = "cp%*s %llu %llu %llu %llu %llu %llu %llu %llu";
            unsigned long long usr, nic, sys, idle, iowait, irq, softirq, steal;
            int ret = std::sscanf(line.data(), fmt, &usr, &nic, &sys, &idle, &iowait, &irq, &softirq, &steal);
            if (ret == 8) {
                unsigned long long total{0};
                unsigned long long busy{0};

                total = usr + nic + sys + idle + iowait + irq + softirq + steal;
                busy = total - idle - iowait;

                load = (busy - m_lastCpuBusy) / static_cast<float>(total - m_lastCpuTotal);
                m_lastCpuBusy = busy;
                m_lastCpuTotal = total;
            }
        } else {
            std::cout << "warning: read cpu load";
        }
    }

    return load;
}

void ProcessInfoReader::update() {
    m_sample.lastCPULoad = readCpuLoad() * 100.0f;  // depends on previous value
    m_sample.lastMemoryUsed = readUsedMemory();
    readProcessInfo(m_sample.lastProcessCount, m_sample.lastThreadCount);
    m_sample.timeStamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
}

float ProcessInfoReader::getLastCPULoad() const {
    return m_sample.lastCPULoad;
}

float ProcessInfoReader::getLastMemoryUsed() const {
    return m_sample.lastMemoryUsed;
}

unsigned int ProcessInfoReader::getLastProcessCount() const {
    return m_sample.lastProcessCount;
}

unsigned int ProcessInfoReader::getLastThreadCount() const {
    return m_sample.lastThreadCount;
}

unsigned long ProcessInfoReader::getTimestamp() const {
    return m_sample.timeStamp;
}

const ProcessSample& ProcessInfoReader::getLastProcessSample() const {
    return m_sample;
}
