#include "systemmonitor.h"
#include "shareddata.h"

#include <iostream>
#include <cstring>
#include <iomanip>
#include <charconv>
#include <functional>
#include <optional>

#include<sys/socket.h>
#include<netinet/in.h>
#include <arpa/inet.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>

#include "systemmonitorreader.h"

SystemMonitorBase* SystemMonitorBase::parse(const std::vector<std::string_view> &args) {
    SystemMonitorBase* result{nullptr};
    unsigned int sampleInterval_ms{DEFAULT_SAMPLE_INTERVAL_MS};
    unsigned int timePeriod_ms{DEFAULT_TIME_PERIOD_MS};
    bool dump{false};
    SystemMonitorBase::MonitorType monitorType{SystemMonitorBase::MonitorType::Socket};

    for (int i = 0; i < args.size(); ++i) {
        if ((args[i] == ARG_PERIOD) && (i + 1) < args.size()) {
            std::from_chars(args[i+1].data(), args[i+1].data() + args[i+1].size(), timePeriod_ms);
            ++i;
        } else if ((args[i] == ARG_SAMPLE_HZ) && (i + 1) < args.size()) {
            double sample_hz{0.0};
            std::from_chars(args[i+1].data(), args[i+1].data() + args[i+1].size(), sample_hz);
            sampleInterval_ms = 1000.0 / sample_hz + 0.5;
            ++i;
        } else if ((args[i] == ARG_VERBOSE)) {
            dump = true;
        } else if ((args[i] == ARG_HELP)) {
            printUsage();
        } else if ((args[i] == ARG_SOCKET)) {
            monitorType = SystemMonitorBase::MonitorType::Socket;
        } else if ((args[i] == ARG_SHMEM)) {
            monitorType = SystemMonitorBase::MonitorType::SharedMemory;
        }
    }

    if (sampleInterval_ms < MIN_SAMPLE_INTERVAL_MS) {
        std::cout << "too small sample time (" << sampleInterval_ms
                  << ") Should be more then: " << MIN_SAMPLE_INTERVAL_MS << std::endl;
        printUsage();
        return nullptr;
    }

    unsigned int sampleCount = timePeriod_ms / sampleInterval_ms;
    if ((timePeriod_ms % sampleInterval_ms) != 0) {
        ++sampleCount;
    }

    if (monitorType == SystemMonitorBase::MonitorType::SharedMemory) {
        std::cout << "shmem serice" << std::endl;
        result = new SystemMonitorSharedMem();
    } else if (monitorType == SystemMonitorBase::MonitorType::Socket) {
        std::cout << "socket serice" << std::endl;
        result = new SystemMonitorSockets();
    }

    result->setSampleInterval_ms(sampleInterval_ms);
    result->setTimePeriod_ms(timePeriod_ms);
    result->setSize(sampleCount);
    result->setDump(dump);

    std::cout << "interval, ms: " << sampleInterval_ms << std::endl;
    std::cout << "peiod, ms: " << timePeriod_ms << std::endl;
    std::cout << "samples count: " << sampleCount << std::endl;

    return result;
}

void SystemMonitorBase::setSampleInterval_ms(unsigned int newSampleInterval_ms)
{
    m_sampleInterval_ms = newSampleInterval_ms;
}

void SystemMonitorBase::setTimePeriod_ms(unsigned int newTimePeriod_ms)
{
    m_timePeriod_ms = newTimePeriod_ms;
}

void SystemMonitorBase::setSize(unsigned int newSize)
{
    m_size = newSize;
}

void SystemMonitorBase::setDump(bool newDump)
{
    m_dump = newDump;
}

void SystemMonitorBase::printUsage() {
    std::cout << "usage: system_monitor_service [option]" << std::endl
              << "  options: " << std::endl
              << "    -t period_ms,  Sets time period in ms for stored samples. Default: 1000" << std::endl
              << "    -s sample_hz,  Sets sample frequency in Hz. Default 24" << std::endl
              << "    -v,  dumps samples" << std::endl
              << "    -h,  print this message" << std::endl
              << "    --socket,  use socket service" << std::endl
              << "    --shmem,  use shared memory service" << std::endl;
}

void SystemMonitorSharedMem::exec() {
    m_run = true;
    sampleLoop();
}

void SystemMonitorSharedMem::stop() {
    // shared mem
    m_run = false;
}

void SystemMonitorBase::dumpProcessSamples() const {
    std::cout << "[" << std::setw(6) << "CPU ]"
              << "[" << std::setw(6) << "MEM ]"
              << "[" << std::setw(6) << "PROC ]"
              << "[" << std::setw(6) << "TH ]"
              << "[" << std::setw(6) << "TIME ]" << std::endl;

    std::function<void(unsigned int i)> print = [this](unsigned int i) {
        std::cout << "[" << std::setw(7) << std::fixed << std::setprecision(3) << m_sampleData[i].lastCPULoad << "]"
                  << "[" << std::setw(7) << m_sampleData[i].lastMemoryUsed << "]"
                  << "[" << std::setw(7) << m_sampleData[i].lastProcessCount << "]"
                  << "[" << std::setw(7) << m_sampleData[i].lastThreadCount << "]"
                  << "[" << m_sampleData[i].timeStamp << "]"
                  << "[" << std::setw(6) << i << "]" << std::endl;
    };

    for (int i = m_sharedData->index; i >= 0; --i) {
        print(i);
    }

    for (unsigned int i = (m_sharedData->samplesCount - 1); i > m_index; --i) {
        print(i);
    }
}

void SystemMonitorBase::sampleLoop() {
    std::unique_lock<std::mutex> lock(m_sampleLoopWaitMutex);
    while (m_run) {
        if (m_index >= m_sharedData->samplesCount) {
            m_index = 0;
        }
        // std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        m_reader.update();
        // std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        // std::cout << "dt = " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "[ms]" << std::endl;
        if (m_dump) {
            dumpProcessSamples();
        }

        int rc = pthread_rwlock_wrlock(&m_sharedData->samplesDataRwLock);
        if (rc == 0) {
            m_sampleData[m_index] = m_reader.getLastProcessSample();
            m_sharedData->index = m_index;

            pthread_rwlock_unlock(&m_sharedData->samplesDataRwLock);
        } else {
            std::cout << "warning: error pthread_rwlock_wrlock" << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(m_sampleInterval_ms));
        ++m_index;
    }
}

bool SystemMonitorSharedMem::setup() {
    m_shmFixed = shm_open(SHARED_MEMORY_OBJECT_NAME, O_CREAT | O_RDWR, 0777);
    if (m_shmFixed == -1) {
        std::cout << "shm_open error" << std::endl;
        return false;
    }

    unsigned int samplesByteSize  = m_size * sizeof(ProcessSample);

    int rc = ftruncate(m_shmFixed, m_fixedMemSize + samplesByteSize);
    if (rc == -1) {
        std::cout << "ftruncate error" << std::endl;
        return false;
    }

    void* p = mmap(0, m_fixedMemSize + samplesByteSize, PROT_WRITE | PROT_READ, MAP_SHARED, m_shmFixed, 0);
    if (p == (void*)-1) {
        std::cout << "mmap error" << std::endl;
        return false;
    }

    m_sharedData = static_cast<SharedData*>(p);

    pthread_rwlockattr_t rwlattr;
    std::cout << "Init shared memory and read/write lock";
    memset(m_sharedData, 0, m_fixedMemSize + samplesByteSize);

    rc = pthread_rwlockattr_init(&rwlattr);
    std::cout << "pthread_rwlockattr_init rc: " << rc << std::endl;
    rc = pthread_rwlockattr_setkind_np(&rwlattr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);
    std::cout << "pthread_rwlockattr_setkind_np rc: " << rc << std::endl;
    rc = pthread_rwlockattr_setpshared(&rwlattr, PTHREAD_PROCESS_SHARED);
    std::cout << "pthread_rwlockattr_setpshared rc: " << rc << std::endl;

    rc = pthread_rwlock_init(&m_sharedData->samplesDataRwLock, &rwlattr);
    std::cout << "pthread_rwlock_init rc: " << rc << std::endl;

    m_sharedData->samplesByteSize  = samplesByteSize;
    m_sharedData->samplesCount = samplesByteSize / sizeof(ProcessSample);
    m_sampleData = reinterpret_cast<ProcessSample*>(&m_sharedData[1]);

    return true;
}

void SystemMonitorSharedMem::closeMonitor() {
    if (m_sharedData != nullptr) {
        munmap(m_sharedData, m_fixedMemSize);
        close(m_shmFixed);
        shm_unlink(SHARED_MEMORY_OBJECT_NAME);
    }
}

void SystemMonitorSockets::startSamplerThread() {
    m_run = true;
    m_samplerThread = std::thread(&SystemMonitorSockets::sampleLoop, this);
    m_samplerThread.detach();
}

void SystemMonitorSockets::setupSocketMem() {
    unsigned int samplesByteSize  = m_size * sizeof(ProcessSample);
    m_sharedData = new SharedData;

    pthread_rwlockattr_t rwlattr;
    std::cout << "Init shared memory and read/write lock";
    memset(m_sharedData, 0, sizeof(SharedData));

    int rc = pthread_rwlockattr_init(&rwlattr);
    std::cout << "pthread_rwlockattr_init rc: " << rc << std::endl;
    rc = pthread_rwlockattr_setkind_np(&rwlattr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);
    std::cout << "pthread_rwlockattr_setkind_np rc: " << rc << std::endl;
    rc = pthread_rwlockattr_setpshared(&rwlattr, PTHREAD_PROCESS_PRIVATE);
    std::cout << "pthread_rwlockattr_setpshared rc: " << rc << std::endl;

    rc = pthread_rwlock_init(&m_sharedData->samplesDataRwLock, &rwlattr);
    std::cout << "pthread_rwlock_init rc: " << rc << std::endl;

    m_sharedData->samplesByteSize  = samplesByteSize;
    m_sharedData->samplesCount = samplesByteSize / sizeof(ProcessSample);
    m_sampleData = new ProcessSample[m_sharedData->samplesCount];
}

void SystemMonitorSockets::clearSocketMem() {
    std::cout << "clear mem" << std::endl;
    if (m_sharedData != nullptr) {
        delete m_sharedData;
    }

    if (m_sampleData != nullptr) {
        delete[] m_sampleData;
    }
}

void SystemMonitorSockets::stop() {
    // socket
    m_runSocketServer = false;
    m_run = false;
    m_sampleLoopWaitMutex.lock();
}

void SystemMonitorSockets::exec() {
    startSamplerThread();
    execSocketServer();
}

void SystemMonitorSockets::closeMonitor() {
    std::cout << "close socket" << std::endl;
    close(m_serverSocket);
    clearSocketMem();
}

bool SystemMonitorSockets::setup() {
    bool result = setupSocket();
    if (result) {
        setupSocketMem();
    }

    return result;
}

bool SystemMonitorSockets::setupSocket() {
    sockaddr_in serverAddr;

    m_serverSocket = socket(PF_INET, SOCK_STREAM, 0);

    int enable = 1;
    if(setsockopt(m_serverSocket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) == -1) {
        std::cout << "error: setsockopt" << std::endl;
        return false;
    }
    if(setsockopt(m_serverSocket, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(enable)) == -1) {
        std::cout << "error: setsockopt" << std::endl;
        return false;
    }

    serverAddr.sin_family = AF_INET;
    serverAddr.sin_addr.s_addr = INADDR_ANY;
    serverAddr.sin_port = htons(SOCKET_PORT);

    //Bind the address struct to the socket
    if (bind(m_serverSocket, reinterpret_cast<sockaddr*>(&serverAddr), sizeof(serverAddr)) == -1) {
        std::cout << "error: bind" << std::endl;
        return false;
    }

    if (listen(m_serverSocket, MAX_SOCKET_CONNECTIONS) != 0) {
        std::cout << "error: listen" << std::endl;
        return false;
    }

    return true;
}

void SystemMonitorSockets::queryThread(int socket) {
    ++m_queryThreadCount;

    Query query;
    Answer ans;
    ans.result = false;

    SystemMonitorReader reader(m_sampleData, m_sharedData, m_sharedData->samplesCount);
    if (recv(socket, &query, sizeof(query), 0) == sizeof(query)) {
        int rc = pthread_rwlock_rdlock(&m_sharedData->samplesDataRwLock);
        if (rc == 0) {
            bool rangeCheckPasses{true};
            std::optional<std::pair<unsigned long, unsigned long>> fromTo;
            switch (query.queryType) {
            case QueryType::MAX_CPU_RANGE:  // fallthrough
            case QueryType::MIN_CPU_RANGE:  // fallthrough
            case QueryType::MAX_MEM_RANGE:  // fallthrough
            case QueryType::MIN_MEM_RANGE: {
                fromTo = {query.from, query.to};
                unsigned long last = m_sampleData[m_sharedData->index].timeStamp;
                unsigned long first;
                if (m_sharedData->index == (m_sharedData->samplesCount - 1)) {
                    first = m_sampleData[0].timeStamp;
                } else {
                    first = m_sampleData[m_sharedData->index + 1].timeStamp;
                }

                if (last < query.from && last < query.to) {
                    rangeCheckPasses = false;
                }

                if (first > query.from && first > query.to) {
                    rangeCheckPasses = false;
                }

                if (query.from < query.to) {
                    rangeCheckPasses = false;
                }

                if (!rangeCheckPasses) {
                    std::cout << "out of range: " << last << "|" << first << std::endl;
                }

                break;
            }
            default: {
                // no need to check
                break;
            }
            }

            if (rangeCheckPasses) {
                unsigned int index{0};
                ans.result = true;
                switch (query.queryType) {
                    case QueryType::NO_QUERY: {
                        ans.result = false;
                        return;
                    }
                    case QueryType::MIN_THREADS: {
                        index = reader.getMinThreadCountIndex();
                        break;
                    }
                    case QueryType::MAX_THREADS: {
                        index = reader.getMaxThreadCountIndex();
                        break;
                    }
                    case QueryType::MAX_CPU: {
                        index = reader.getMaxMemoryUsedIndex(query.from, query.to);
                        break;
                    }
                    case QueryType::MIN_CPU: {
                        index = reader.getMinCPULoadIndex();
                        break;
                    }
                    case QueryType::MAX_CPU_RANGE: {
                        index = reader.getMaxCPULoadIndex(query.from, query.to);
                        break;
                    }
                    case QueryType::MIN_CPU_RANGE: {
                        index = reader.getMinCPULoadIndex(query.from, query.to);
                        break;
                    }
                    case QueryType::MAX_MEM: {
                        index = reader.getMaxMemoryUsedIndex();
                        break;
                    }
                    case QueryType::MIN_MEM: {
                        index = reader.getMinMemoryUsedIndex();
                        break;
                    }
                    case QueryType::MAX_MEM_RANGE: {
                        index = reader.getMaxMemoryUsedIndex(query.from, query.to);
                        break;
                    }
                    case QueryType::MIN_MEM_RANGE: {
                        index = reader.getMinMemoryUsedIndex(query.from, query.to);
                        break;
                    }
                } // switch query

                ans.sample = m_sampleData[index];
            } // range
        } // locked
        pthread_rwlock_unlock(&m_sharedData->samplesDataRwLock);

        send(socket, &ans, sizeof(Answer), 0);
        close(socket);
    }

    --m_queryThreadCount;
    m_queryWaitCV.notify_all();
}

void SystemMonitorSockets::execSocketServer() {
    int i = {0};
    sockaddr_storage serverStorage;
    socklen_t addr_size = sizeof(serverStorage);
    int acceptRetries{10};

    while (m_runSocketServer) {
        int incomingSocket{-1};
        incomingSocket = accept(m_serverSocket, reinterpret_cast<sockaddr*>(&serverStorage), &addr_size);
        if (incomingSocket == -1) {
            --acceptRetries;
            if (acceptRetries == 0) {
                std::cout << "error: incoming connection" << std::endl;
                break;
            } else {
                continue;
            }
        }

        m_socketConnectionThreads[i] = std::thread(&SystemMonitorSockets::queryThread, this, incomingSocket);
        m_socketConnectionThreads[i].detach();

        if (i >= (MAX_SOCKET_CONNECTIONS - 1)) {
            std::unique_lock<std::mutex> lock(m_queryWiatMutex);
            while (m_queryThreadCount >= MAX_SOCKET_CONNECTIONS) {
                m_queryWaitCV.wait_for(lock, std::chrono::milliseconds(100));
            }
            i = m_queryThreadCount;
        } else {
            ++i;
        }
    }
}
