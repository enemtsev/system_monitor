#ifndef SYSTEMMONITORREADER_H
#define SYSTEMMONITORREADER_H

#include "shareddata.h"
#include <functional>

class SystemMonitorReader {
public:
    SystemMonitorReader(const ProcessSample *sampleData,
                        const SharedData *sharedData,
                        unsigned long size);

    unsigned int getMaxThreadCountIndex() const;
    unsigned int getMinThreadCountIndex() const;

    unsigned int getMinCPULoadIndex(unsigned long from, unsigned long to) const;
    unsigned int getMaxCPULoadIndex(unsigned long from, unsigned long to) const;
    unsigned int getMinCPULoadIndex() const;
    unsigned int getMaxCPULoadIndex() const;

    unsigned int getMinMemoryUsedIndex(unsigned long from, unsigned long to) const;
    unsigned int getMaxMemoryUsedIndex(unsigned long from, unsigned long to) const;
    unsigned int getMinMemoryUsedIndex() const;
    unsigned int getMaxMemoryUsedIndex() const;

private:
    unsigned int getIndex(std::function<bool(const ProcessSample &s1, const ProcessSample &s2)> cmp) const;
    unsigned int getIndexRange(unsigned long from, unsigned long to, unsigned int index,
                               std::function<bool(const ProcessSample &s1, const ProcessSample &s2)> cmp) const;
private:
    const ProcessSample *m_sampleData;
    const SharedData *m_sharedData;
    const unsigned long m_size;

private:
    SystemMonitorReader() = delete;
    SystemMonitorReader(const SystemMonitorReader &) = delete;
    SystemMonitorReader(const SystemMonitorReader &&) = delete;
    SystemMonitorReader& operator = (const SystemMonitorReader &) = delete;
    SystemMonitorReader& operator = (const SystemMonitorReader &&) = delete;
};

#endif // SYSTEMMONITORREADER_H
