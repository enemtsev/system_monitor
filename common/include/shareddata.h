#ifndef SHAREDDATA_H
#define SHAREDDATA_H

#include <pthread.h>
#include <atomic>
#include <limits>

constexpr char SHARED_MEMORY_OBJECT_NAME[] {"rimac_systemmonitor"};

static constexpr uint16_t SOCKET_PORT{8889};
static constexpr char SOCKET_ADDR[] {"127.0.0.1"};

enum class QueryType {
    NO_QUERY,
    MIN_THREADS,
    MAX_THREADS,
    MAX_CPU,
    MIN_CPU,
    MAX_CPU_RANGE,
    MIN_CPU_RANGE,
    MAX_MEM,
    MIN_MEM,
    MAX_MEM_RANGE,
    MIN_MEM_RANGE
};

struct ProcessSample {
    float lastCPULoad{0.0f};
    float lastMemoryUsed{0.0f};
    unsigned int lastProcessCount{0};
    unsigned int lastThreadCount{0};
    unsigned long timeStamp{0};
};

struct SharedData {
    // rw lock for shared memory
    pthread_rwlock_t samplesDataRwLock;
    // last sample index in the array
    unsigned int index{0};
    // samples array memory size
    unsigned long samplesByteSize{0};
    // samples count
    unsigned long samplesCount{0};
};

struct Query {
    QueryType queryType;
    unsigned long from;
    unsigned long to;
};

struct Answer {
    ProcessSample sample;
    bool result;
};

#endif // SHAREDDATA_H
