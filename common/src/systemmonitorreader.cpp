#include "systemmonitorreader.h"
#include <iostream>

SystemMonitorReader::SystemMonitorReader(const ProcessSample *sampleData,
                                         const SharedData *sharedData,
                                         unsigned long size)
    : m_sampleData{sampleData}, m_sharedData{sharedData}, m_size{size} {
}

unsigned int SystemMonitorReader::getIndex(std::function<bool(const ProcessSample &s1, const ProcessSample &s2)> cmp) const {
    unsigned int index = 0;
    for (unsigned int i = 1; i < m_size; ++i) {
        if (cmp(m_sampleData[index], m_sampleData[i])) {
            index = i;
        }
    }

    return index;
}

unsigned int SystemMonitorReader::getIndexRange(unsigned long from, unsigned long to, unsigned int index,
                           std::function<bool(const ProcessSample &s1, const ProcessSample &s2)> cmp) const {
    unsigned int cmpIndex = index;
    bool inRange1{false};
    bool checkPart2{true};
    for (int i = index; i >= 0; --i) {
        unsigned long timeStamp = m_sampleData[i].timeStamp;
        if ((timeStamp <= from) && (timeStamp >= to)) {
            if (!inRange1) {
                cmpIndex = i;
                inRange1 = true;
            } else if (cmp(m_sampleData[cmpIndex], m_sampleData[i])) {
                cmpIndex = i;
            } else {
                // nothing to do
            }
        } else if (inRange1) {
            checkPart2 = false;
            // got out of range
            break;
        } else {
            // nothing to do
        }
    }

    // do if we need to check other part of circular buffer
    if (checkPart2) {
        bool inRange2 = false;
        for (int i = index + 1; i < m_size; ++i) {
            unsigned long timeStamp = m_sampleData[i].timeStamp;
            if ((timeStamp <= from) && (timeStamp >= to)) {
                if (!inRange1 && !inRange2) {
                    cmpIndex = index;
                    inRange2 = true;
                } else if (cmp(m_sampleData[cmpIndex], m_sampleData[i])) {
                    cmpIndex = i;
                } else {
                    // nothing to do
                }
            } else if (inRange2) {
                // got out of range
                break;
            } else {
                // nothing to do
            }
        }
    }

    return cmpIndex;
}

unsigned int SystemMonitorReader::getMaxThreadCountIndex() const{
    return getIndex([](const ProcessSample &s1, const ProcessSample &s2) {
       return (s1.lastThreadCount < s2.lastThreadCount);
    });
}

unsigned int SystemMonitorReader::getMinThreadCountIndex() const{
    return getIndex([](const ProcessSample &s1, const ProcessSample &s2) {
       return (s1.lastThreadCount > s2.lastThreadCount);
    });

}

unsigned int SystemMonitorReader::getMinCPULoadIndex(unsigned long from, unsigned long to) const {
    return getIndexRange(from, to, m_sharedData->index,
        [](const ProcessSample &s1, const ProcessSample &s2) {
            return (s1.lastCPULoad > s2.lastCPULoad);
        });
}

unsigned int SystemMonitorReader::getMaxCPULoadIndex(unsigned long from, unsigned long to) const {
    return getIndexRange(from, to, m_sharedData->index,
        [](const ProcessSample &s1, const ProcessSample &s2) {
            return (s1.lastCPULoad < s2.lastCPULoad);
        });
}

unsigned int SystemMonitorReader::getMinCPULoadIndex() const {
    return getIndex([](const ProcessSample &s1, const ProcessSample &s2) {
        return (s1.lastCPULoad > s2.lastCPULoad);
    });
}

unsigned int SystemMonitorReader::getMaxCPULoadIndex() const {
    return getIndex([](const ProcessSample &s1, const ProcessSample &s2) {
        return (s1.lastCPULoad < s2.lastCPULoad);
    });
}

unsigned int SystemMonitorReader::getMinMemoryUsedIndex(unsigned long from, unsigned long to) const {
    return getIndexRange(from, to, m_sharedData->index,
        [](const ProcessSample &s1, const ProcessSample &s2) {
            return (s1.lastMemoryUsed > s2.lastMemoryUsed);
        });
}

unsigned int SystemMonitorReader::getMaxMemoryUsedIndex(unsigned long from, unsigned long to) const {
    return getIndexRange(from, to, m_sharedData->index,
        [](const ProcessSample &s1, const ProcessSample &s2) {
            return (s1.lastMemoryUsed < s2.lastMemoryUsed);
        });
}

unsigned int SystemMonitorReader::getMinMemoryUsedIndex() const {
    return getIndex([](const ProcessSample &s1, const ProcessSample &s2) {
                        return (s1.lastMemoryUsed > s2.lastMemoryUsed);
                    });
}

unsigned int SystemMonitorReader::getMaxMemoryUsedIndex() const {
    return getIndex([](const ProcessSample &s1, const ProcessSample &s2) {
                        return (s1.lastMemoryUsed < s2.lastMemoryUsed);
                    });
}
